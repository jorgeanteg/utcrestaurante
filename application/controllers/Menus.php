<?php


    class Menus extends CI_Controller
    {
        function __construct()
        {
            //code
            parent::__construct();
        }

        //Renderisacion de la vista que 
        //muestra los desayunos

        public function desayunos(){
            $this->load->view('header');
            $this->load->view('menus/desayunos');
            $this->load->view('footer');
        }

        public function almuerzos(){
            $this->load->view('header');
            $this->load->view('menus/almuerzos');
            $this->load->view('footer');
        }

        public function meriendas(){
            $this->load->view('header');
            $this->load->view('menus/meriendas');
            $this->load->view('footer');
        }

        public function cartas(){
            $this->load->view('header');
            $this->load->view('menus/cartas');
            $this->load->view('footer');
        }


        public function ubicanos(){
            $this->load->view('header');
            $this->load->view('menus/ubicanos');
            $this->load->view('footer');
        }

        public function visiones(){
            $this->load->view('header');
            $this->load->view('menus/visiones');
            $this->load->view('footer');
        }

        public function misiones(){
            $this->load->view('header');
            $this->load->view('menus/misiones');
            $this->load->view('footer');
        }
    }

?>