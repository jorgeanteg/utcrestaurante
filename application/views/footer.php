
<style>
    footer{
        background-color: #333333;
        color: white;
    }
</style>
<footer>
    <div class="container">
        <div class="row">
            <br>
            <div class="col-md-6 text-center">
                <h4>Desarrollado por: </h4>
                <p>Jorge Ante</p>
            </div>
            <div class="col-md-6 text-center">
                <h4>Ecuador</h4>
                <p>Cotopaxi</p>
            </div>

        </div>
        <div class="row">
            <hr>
            <div class="col-md-12 text-center">
                <p>&copy;2023 Todos los derechos reservados</p>
            </div>

        </div>
    </div>
</footer>



</body>

</html>