
<style>
    .color-btn {
        background-color: #333333;
        color: white;
    }

    .color-btn:hover {
        background-color: green;
        color: white;
    }
</style>


<div class="container">
    <br>
    <h1 class="text-center">PLATOS A LA CARTA</h1>
    <br>
    <div class="row">
        <div class=" col-md-6">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/20.jpg" alt="">
                <div class="caption">
                    <h3>PULPO A LA GALLEGA</h3>
                    <p>Acompañado de papas panaderas con pimentón de la vera picante o dulce y aceite de oliva.</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class=" col-md-6">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/21.jpg" alt="">
                <div class="caption">
                    <h3>PAELLA CAMPERA</h3>
                    <p>Deliciosa paella con variedad de carnes: pollo, cerdo y chistorra.</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class=" col-md-6">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/22.jpg" alt="">
                <div class="caption">
                    <h3>CHULETÓN VASCO</h3>
                    <p>Ojo de bife madurado acompañado con papas fritas y ensalada.</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class=" col-md-6">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/23.jpg" alt="">
                <div class="caption">
                    <h3>JARRETE DE CORDERO</h3>
                    <p>Acompañado de compota de manzana y ensalda orgánica.</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
       
    </div>
</div>