
<style>
    .color-btn {
        background-color: #333333;
        color: white;
    }

    .color-btn:hover {
        background-color: green;
        color: white;
    }
</style>


<div class="container">
    <br>
    <h1 class="text-center">DESAYUNOS</h1>
    <br>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="">
                <div class="caption">
                    <h3>TORTILLA ESPAÑOLA</h3>
                    <p>Exquisita tortilla al estilo español con patatas y cebollas caramelizadas.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/2.jpg" alt="">
                <div class="caption">
                    <h3>BOLONES</h3>
                    <p>Bolones de chicharron y queso con hevo frito.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/3.jpg" alt="">
                <div class="caption">
                    <h3>ENSALADA MAITANE</h3>
                    <p>Lechugas variadas, queso, tomates, pepas de zambo, verduras o frutas sugeridas por el Chef, acompañados con vinagretas de la casa.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/6.jpg" alt="">
                <div class="caption">
                    <h3>FILETE DE PESCA DEL DÍA</h3>
                    <p>Filete de pescado fresco al horno con papas panaderas, zuquini confitado y ensalada fresca.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/4.jpg" alt="">
                <div class="caption">
                    <h3>HUEVOS ROTOS</h3>
                    <p>Huevos rotos con patatas fritas con jamón o chistorra.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/5.jpg" alt="">
                <div class="caption">
                    <h3>ARROZ CALDOSO</h3>
                    <p>Arroz jugoso con langostino, mejillones, almejas blancas, pescado, calamar y camarones.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
       
    </div>
</div>