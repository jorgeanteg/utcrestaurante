
<style>
    .color-btn {
        background-color: #333333;
        color: white;
    }

    .color-btn:hover {
        background-color: green;
        color: white;
    }
</style>


<div class="container">
    <br>
    <h1 class="text-center">ALMUERZOS</h1>
    <br>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/7.jpg" alt="">
                <div class="caption">
                    <h3>PATATAS BRAVAS</h3>
                    <p>Patatas con pimentón de la vera picante o dulce con cebollas confitadas al estilo castellano.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/8.jpg" alt="">
                <div class="caption">
                    <h3>LANGOSTINOS A LA BRASA</h3>
                    <p>Langostinos a la brasa con nuestra salsa de pimentón, acompañado de papas panaderas y ensalada orgánica.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/9.jpg" alt="">
                <div class="caption">
                    <h3>SOPA DE VEGETALES</h3>
                    <p>Exquisita sopa de vegetales frescos de la huerta.</p>
                    <p><b>Precio: </b>$4</p>
                    <br>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/10.jpg" alt="">
                <div class="caption">
                    <h3>GUISO DE GARBANZO</h3>
                    <p>Exquisito plato acompañado con carne beyond y arroz.</p>
                    <br>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/11.jpg" alt="">
                <div class="caption">
                    <h3>CAMARONES AL AJILLO</h3>
                    <p>Con base de papas panaderas en su salsa.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" height="585px" src="<?php echo base_url(); ?>/assets/images/5.jpg" alt="">
                <div class="caption">
                    <h3>ARROZ CALDOSO</h3>
                    <p>Arroz jugoso con langostino, mejillones, almejas blancas, pescado, calamar y camarones.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
       
    </div>
</div>