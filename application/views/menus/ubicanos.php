<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>UBÍCANOS</h1>
            <p>Estamos ubicados en la Univertidad Ténica de Cotopaxi</p>
            <div id="mapaDireccion" style="width:100%; height:500px;">
            </div>
        </div>
    </div>

</div>
<br>

<script type="text/javascript">
    function initMap() {

        // creando el punto central del mapa
        var coordenadaCentral = new google.maps.LatLng(-0.840997, -78.671649);

        //Creando mapa
        var mapa1 = new google.maps.Map(document.getElementById('mapaDireccion'), { center: coordenadaCentral, zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP });
    }
</script>