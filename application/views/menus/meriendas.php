
<style>
    .color-btn {
        background-color: #333333;
        color: white;
    }

    .color-btn:hover {
        background-color: green;
        color: white;
    }
</style>


<div class="container">
    <br>
    <h1 class="text-center">MERIENDAS</h1>
    <br>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%"  src="<?php echo base_url(); ?>/assets/images/13.jpg" alt="">
                <div class="caption">
                    <h3>PULPO A LA GALLEGA</h3>
                    <p>Acompañado de papas panaderas con pimentón de la vera picante de sal o dulce  con miel y aceite de oliva.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" src="<?php echo base_url(); ?>/assets/images/14.jpg" alt="">
                <div class="caption">
                    <h3>TARTAR DE ATÚN ROJO</h3>
                    <p>Con cama de aguacate, brotes de la casa y aromatizado en aceite de trufa.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" src="<?php echo base_url(); ?>/assets/images/15.jpg" alt="">
                <div class="caption">
                    <h3>PAELLA DE MARISCOS</h3>
                    <p>Arroz con langostino, mejillones, almejas blancas, pescado, calamar y camarones, preparado con azafrán español.</p>
                    <p><b>Precio: </b>$4</p>
                    <br>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" src="<?php echo base_url(); ?>/assets/images/16.jpg" alt="">
                <div class="caption">
                    <h3>PAELLA VEGETARIANA</h3>
                    <p>Deliciosa paella con variedad de vegetales, hongos y portobellos, zuquini, pimiento rojo, zanahoria, cebolla.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" src="<?php echo base_url(); ?>/assets/images/17.jpg" alt="">
                <div class="caption">
                    <h3>CHULETÓN VASCO</h3>
                    <p>Ojo de bife madurado acompañado con papas fritas y ensalada.</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <img width="100%" src="<?php echo base_url(); ?>/assets/images/18.jpg" alt="">
                <div class="caption">
                    <h3>OSTRAS (POR TEMPORADA)</h3>
                    <p>Ostras frescas con un toque de salsa tabasco, salsa inglesa, limón acompañado de pico de gallo</p>
                    <p><b>Precio: </b>$4</p>
                    <p class="text-center "><a href="#" class="btn color-btn" role="button">Ordenar</a> </p>
                </div>
            </div>
        </div>
       
    </div>
</div>